/**
 * 
 */
package uo.harish.prakash.sycamore.batch.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JFrame;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.gui.SycamoreSystem.TIMELINE_MODE;
import it.diunipi.volpi.sycamore.plugins.SycamorePlugin;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditions;
import it.diunipi.volpi.sycamore.plugins.measures.Measure;
import it.diunipi.volpi.sycamore.plugins.schedulers.Scheduler;
import it.diunipi.volpi.sycamore.plugins.visibilities.Visibility;
import uo.harish.prakash.sycamore.batch.core.SycamoreBatch;
import uo.harish.prakash.sycamore.batch.core.SycamoreBatchExtensions;
import uo.harish.prakash.sycamore.batch.data.SycamoreBatchRobotListConfiguration;
import uo.harish.prakash.sycamore.batch.utilities.SycamoreBatchPluginManager;

/**
 * @author harry
 *
 */
public class SycamoreBatchMainWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private SycamoreBatchSelectionPanel				_selectionPanel;
	private SycamoreBatchConsolePanel				_consolePanel;
	private SycamoreBatchCommandPanel				_commandPanel;
	private SycamoreBatchRobotConfigurationPanel	_robotConfigurationPanel;
	private SycamoreBatchMeasureConfigurationPanel	_measureConfigurationPanel;
	private SycamoreBatch							_batchSimulator;

	private SycamoreBatchDialogBox	_robotConfigurationDialog;
	private SycamoreBatchDialogBox	_measureConfigurationDialog;

	private File						_userFolder;
	private SycamoreBatchPluginManager	_pluginManager;
	private SycamoreBatchMainWindow		_self;

	public SycamoreBatchMainWindow(File UserFolder) {

		this._self = this;
		this._userFolder = UserFolder;
		this._pluginManager = new SycamoreBatchPluginManager(new File(UserFolder, "Plugins"));

		setupGUI();
		setTitle("Sycamore Batch");
	}

	/**
	 * @return the _selectionPanel
	 */
	public SycamoreBatchSelectionPanel getSelectionPanel() {

		if (_selectionPanel == null) {
			_selectionPanel = new SycamoreBatchSelectionPanel(getPluginManager());
			_selectionPanel.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					if (e.getActionCommand().equals(SycamoreBatchGUIStatus.CONFIGURE_PLUGIN.name())) {

						if (e.getSource() == null) {
							return;
						}

						if (SycamorePlugin.class.isAssignableFrom(e.getSource().getClass())) {
							SycamorePlugin plugin = SycamorePlugin.class.cast(e.getSource());
							if (plugin.getPanel_settings() == null) {
								return;
							}

							new SycamoreBatchDialogBox(getSelf(), plugin.getPanel_settings(), plugin.getPluginName());
						}
					}

				}
			});
		}
		return _selectionPanel;
	}

	/**
	 * @return the _consolePanel
	 */
	public SycamoreBatchConsolePanel getConsolePanel() {

		if (_consolePanel == null) {
			_consolePanel = new SycamoreBatchConsolePanel(getBatchSimulator());
		}
		return _consolePanel;
	}

	/**
	 * @return the _commandPanel
	 */
	public SycamoreBatchCommandPanel getCommandPanel() {

		if (_commandPanel == null) {
			_commandPanel = new SycamoreBatchCommandPanel();
			_commandPanel.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					if (e.getActionCommand().equals(SycamoreBatchGUIStatus.START_SIMULATION.name())) {
						getCommandPanel().setComponentsEnabled(false);
						getSelectionPanel().setComponentsEnabled(false);
						getRobotConfigurationPanel().setComponentsEnabled(false);
						getMeasureConfigurationPanel().setComponentsEnabled(false);
						BridgeUserInput();
					}

					else if (e.getActionCommand().equals(SycamoreBatchGUIStatus.MANAGE_ROBOTS.name())) {
						getRobotConfigurationDialog().setVisible(true);
					}

					else if (e.getActionCommand().equals(SycamoreBatchGUIStatus.MANAGE_MEASURES.name())) {
						getMeasureConfigurationDialog().setVisible(true);
					}
				}
			});
		}
		return _commandPanel;
	}

	/**
	 * @return the robotConfigurationPanel
	 */
	private SycamoreBatchRobotConfigurationPanel getRobotConfigurationPanel() {

		if (_robotConfigurationPanel == null) {
			_robotConfigurationPanel = new SycamoreBatchRobotConfigurationPanel(getPluginManager());
		}
		return _robotConfigurationPanel;
	}

	/**
	 * @return the _robotConfigurationPanel
	 */
	public SycamoreBatchDialogBox getRobotConfigurationDialog() {

		if (_robotConfigurationDialog == null) {
			_robotConfigurationDialog = new SycamoreBatchDialogBox(getSelf(), getRobotConfigurationPanel(), "Manage Robot Lists", false);
		}

		return _robotConfigurationDialog;
	}

	/**
	 * @return the measureConfigurationPanel
	 */
	private SycamoreBatchMeasureConfigurationPanel getMeasureConfigurationPanel() {

		if (_measureConfigurationPanel == null) {
			_measureConfigurationPanel = new SycamoreBatchMeasureConfigurationPanel(getPluginManager().getLoadedMeasures());
		}
		return _measureConfigurationPanel;
	}

	/**
	 * @return the _measureConfigurationDialog
	 */
	private SycamoreBatchDialogBox getMeasureConfigurationDialog() {

		if (_measureConfigurationDialog == null) {
			_measureConfigurationDialog = new SycamoreBatchDialogBox(getSelf(), getMeasureConfigurationPanel(), "Manage Measures", false);
		}

		return _measureConfigurationDialog;
	}

	/**
	 * @return the _batchSimulator
	 */
	public SycamoreBatch getBatchSimulator() {

		if (_batchSimulator == null) {
			_batchSimulator = new SycamoreBatch();
		}
		return _batchSimulator;
	}

	/**
	 * @return the _userFolder
	 */
	private File getUserFolder() {

		return _userFolder;
	}

	/**
	 * @return the _pluginManager
	 */
	private SycamoreBatchPluginManager getPluginManager() {

		return _pluginManager;
	}

	/**
	 * @return the _self
	 */
	private SycamoreBatchMainWindow getSelf() {

		return _self;
	}

	private void setupGUI() {

		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints c;

		layout.columnWeights = new double[] { 0.1, 0.9 };
		layout.rowWeights = new double[] { 0.6, 0.4 };

		setVisible(true);
		setMinimumSize(new Dimension(800, 600));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(layout);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		add(getSelectionPanel(), c);

		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 0;
		c.gridheight = 2;
		c.fill = GridBagConstraints.BOTH;
		add(getConsolePanel(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.fill = GridBagConstraints.BOTH;
		add(getCommandPanel(), c);

		setResizable(false);
		revalidate();
	}

	@SuppressWarnings("unchecked")
	private void BridgeUserInput() {

		getBatchSimulator().setNumberOfSimulations(getSelectionPanel().getNumberOfSimulations());
		getBatchSimulator().setScheduler((Scheduler<Point2D>) getSelectionPanel().getSelectedScheduler());
		getBatchSimulator().setVisibility((Visibility<Point2D>) getSelectionPanel().getSelectedVisibility());
		getBatchSimulator().setInitialConditions((InitialConditions<Point2D>) getSelectionPanel().getSelectedInitialConditions());

		ArrayList<Measure> selectedMeasures = getMeasureConfigurationPanel().getSelectedMeasures();
		for (Measure measure : selectedMeasures) {
			if (measure != null) {
				getBatchSimulator().addMeasures(measure);
			}
		}

		ArrayList<SycamoreBatchRobotListConfiguration> lists = getRobotConfigurationPanel().getSycamoreBatchRobotListConfigurations();
		for (SycamoreBatchRobotListConfiguration config : lists) {
			if (config != null && config.getAlgorithm() != null) {
				getBatchSimulator().addRobotConfiguration(config);
			}
		}

		SycamoreBatchExtensions.SaveTimelineAfterEachSimulation(
			getBatchSimulator(),
			new File(getUserFolder(), "Projects"),
			TIMELINE_MODE.LIVE);
		getBatchSimulator().StartBatch();
	}

}
