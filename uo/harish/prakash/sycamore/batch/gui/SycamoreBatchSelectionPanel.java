/**
 * 
 */
package uo.harish.prakash.sycamore.batch.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.plugins.SycamorePlugin;
import it.diunipi.volpi.sycamore.plugins.agreements.Agreement;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditions;
import it.diunipi.volpi.sycamore.plugins.memory.Memory;
import it.diunipi.volpi.sycamore.plugins.schedulers.Scheduler;
import it.diunipi.volpi.sycamore.plugins.visibilities.Visibility;
import uo.harish.prakash.sycamore.batch.utilities.SycamoreBatchPluginManager;

/**
 * @author harry
 *
 */
public class SycamoreBatchSelectionPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel	_labelVisibility;
	private JLabel	_labelInitialConditions;
	private JLabel	_labelScheduler;
	private JLabel	_labelMemory;
	private JLabel	_labelAgreement;
	private JLabel	_labelNumberOfSimulations;

	private JButton	_buttonConfigureVisibility;
	private JButton	_buttonConfigureInitialConditions;
	private JButton	_buttonConfigureScheduler;
	private JButton	_buttonConfigureMemory;
	private JButton	_buttonConfigureAgreement;

	private JTextField _textNumberOfSimulations;

	private JComboBox<Visibility<?>>		_comboVisibility;
	private JComboBox<InitialConditions<?>>	_comboInitialConditions;
	private JComboBox<Scheduler<?>>			_comboScheduler;
	private JComboBox<Memory<?>>			_comboMemory;
	private JComboBox<Agreement<?>>			_comboAgreement;

	private SycamoreBatchPluginManager _pluginManager;

	private ArrayList<ActionListener> _listeners;

	public SycamoreBatchSelectionPanel(SycamoreBatchPluginManager pluginManager) {
		this._pluginManager = pluginManager;
		setupGUI();
	}

	private void setupGUI() {

		GridBagConstraints c;
		GridBagLayout layout = new GridBagLayout();

		layout.columnWeights = new double[] { 0.9, 0.1 };
		layout.rowWeights = new double[] { 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 100 };
		setLayout(layout);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 5, 0, 5);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		add(getLabelVisibility(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 0, 5);
		add(getComboVisibility(), c);

		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 0, 5);
		add(getButtonConfigureVisibility(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 5, 0, 5);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		add(getLabelInitialConditions(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 3;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 0, 5);
		add(getComboInitialConditions(), c);

		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 3;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 0, 5);
		add(getButtonConfigureInitialConditions(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 5, 0, 5);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		add(getLabelScheduler(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 5;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 0, 5);
		add(getComboScheduler(), c);

		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 5;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 0, 5);
		add(getButtonConfigureScheduler(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 6;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 5, 0, 5);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		add(getLabelMemory(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 7;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 0, 5);
		add(getComboMemory(), c);

		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 7;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 0, 5);
		add(getButtonConfigureMemory(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 8;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10, 5, 0, 5);
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		add(getLabelAgreement(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 9;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 10, 5);
		add(getComboAgreement(), c);

		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 9;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 10, 5);
		add(getButtonConfigureAgreement(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 10;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 5, 10, 5);
		add(getLabelNumberOfSimulations(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 11;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridwidth = 2;
		c.insets = new Insets(0, 5, 10, 5);
		add(getTextNumberOfSimulations(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 12;
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 2;
		c.insets = new Insets(0, 5, 10, 5);
		add(new JPanel(), c);
	}

	private void fireEvent(ActionEvent e) {

		for (ActionListener listener : getListeners()) {
			listener.actionPerformed(e);
		}
	}

	/**
	 * @return the _labelVisibility
	 */
	public JLabel getLabelVisibility() {

		if (_labelVisibility == null) {
			_labelVisibility = new JLabel("Select Visibility");
		}
		return _labelVisibility;
	}

	/**
	 * @return the _labelInitialConditions
	 */
	public JLabel getLabelInitialConditions() {

		if (_labelInitialConditions == null) {
			_labelInitialConditions = new JLabel("Select Initial Conditions");
		}
		return _labelInitialConditions;
	}

	/**
	 * @return the _labelScheduler
	 */
	public JLabel getLabelScheduler() {

		if (_labelScheduler == null) {
			_labelScheduler = new JLabel("Select Scheduler");
		}
		return _labelScheduler;
	}

	/**
	 * @return the _labelMemory
	 */
	public JLabel getLabelMemory() {

		if (_labelMemory == null) {
			_labelMemory = new JLabel("Select Memory");
		}
		return _labelMemory;
	}

	/**
	 * @return the _labelAgreement
	 */
	public JLabel getLabelAgreement() {

		if (_labelAgreement == null) {
			_labelAgreement = new JLabel("Select Agreement");
		}
		return _labelAgreement;
	}

	/**
	 * @return the _buttonConfigureVisibility
	 */
	public JButton getButtonConfigureVisibility() {

		if (_buttonConfigureVisibility == null) {
			_buttonConfigureVisibility = new JButton("...");
			_buttonConfigureVisibility.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					fireEvent(new ActionEvent(getComboVisibility().getSelectedItem(), 0, SycamoreBatchGUIStatus.CONFIGURE_PLUGIN.name()));
				}
			});
		}
		return _buttonConfigureVisibility;
	}

	/**
	 * @return the _buttonConfigureInitialConditions
	 */
	public JButton getButtonConfigureInitialConditions() {

		if (_buttonConfigureInitialConditions == null) {
			_buttonConfigureInitialConditions = new JButton("...");
			_buttonConfigureInitialConditions.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					fireEvent(
						new ActionEvent(getComboInitialConditions().getSelectedItem(), 0, SycamoreBatchGUIStatus.CONFIGURE_PLUGIN.name()));
				}
			});
		}
		return _buttonConfigureInitialConditions;
	}

	/**
	 * @return the _buttonConfigureScheduler
	 */
	public JButton getButtonConfigureScheduler() {

		if (_buttonConfigureScheduler == null) {
			_buttonConfigureScheduler = new JButton("...");
			_buttonConfigureScheduler.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					fireEvent(new ActionEvent(getComboScheduler().getSelectedItem(), 0, SycamoreBatchGUIStatus.CONFIGURE_PLUGIN.name()));
				}
			});
		}
		return _buttonConfigureScheduler;
	}

	/**
	 * @return the _buttonConfigureMemory
	 */
	public JButton getButtonConfigureMemory() {

		if (_buttonConfigureMemory == null) {
			_buttonConfigureMemory = new JButton("...");
			_buttonConfigureMemory.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					fireEvent(new ActionEvent(getComboMemory().getSelectedItem(), 0, SycamoreBatchGUIStatus.CONFIGURE_PLUGIN.name()));
				}
			});
		}
		return _buttonConfigureMemory;
	}

	/**
	 * @return the _buttonCongirgureAgreement
	 */
	public JButton getButtonConfigureAgreement() {

		if (_buttonConfigureAgreement == null) {
			_buttonConfigureAgreement = new JButton("...");
			_buttonConfigureAgreement.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					fireEvent(new ActionEvent(getComboAgreement().getSelectedItem(), 0, SycamoreBatchGUIStatus.CONFIGURE_PLUGIN.name()));
				}
			});
		}
		return _buttonConfigureAgreement;
	}

	/**
	 * @return the _labelNumberOfSimulations
	 */
	private JLabel getLabelNumberOfSimulations() {

		if (_labelNumberOfSimulations == null) {
			_labelNumberOfSimulations = new JLabel("Number of Simulations");
		}
		return _labelNumberOfSimulations;
	}

	/**
	 * @return the _textNumberOfSimulations
	 */
	private JTextField getTextNumberOfSimulations() {

		if (_textNumberOfSimulations == null) {

			_textNumberOfSimulations = new JTextField();
			_textNumberOfSimulations.setHorizontalAlignment(JTextField.RIGHT);
		}
		return _textNumberOfSimulations;
	}

	public int getNumberOfSimulations() {

		try {
			int numberOfSimulations = Integer.parseInt(getTextNumberOfSimulations().getText());
			return numberOfSimulations;
		}
		catch (NullPointerException ex) {
			return 0;
		}
		catch (NumberFormatException ex) {
			return 0;
		}
	}

	/**
	 * @return the _comboVisibility
	 */
	public JComboBox<Visibility<?>> getComboVisibility() {

		if (_comboVisibility == null) {
			_comboVisibility = new JComboBox<>(getPluginManager().getLoadedVisibilities(TYPE.TYPE_2D));
			_comboVisibility.insertItemAt(null, 0);
			_comboVisibility.setSelectedIndex(0);
			_comboVisibility.setPreferredSize(new Dimension(200, _comboVisibility.getPreferredSize().height));
			setPluginConfig((SycamorePlugin) _comboVisibility.getSelectedItem(), getButtonConfigureVisibility());
			_comboVisibility.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					setPluginConfig((SycamorePlugin) _comboVisibility.getSelectedItem(), getButtonConfigureVisibility());
				}
			});
		}
		return _comboVisibility;
	}

	public Visibility<?> getSelectedVisibility() {

		return (Visibility<?>) getComboVisibility().getSelectedItem();
	}

	/**
	 * @return the _comboInitialConditions
	 */
	public JComboBox<InitialConditions<?>> getComboInitialConditions() {

		if (_comboInitialConditions == null) {
			_comboInitialConditions = new JComboBox<>(getPluginManager().getLoadedInitialConditions(TYPE.TYPE_2D));
			_comboInitialConditions.insertItemAt(null, 0);
			_comboInitialConditions.setSelectedIndex(0);
			_comboInitialConditions.setPreferredSize(new Dimension(200, _comboInitialConditions.getPreferredSize().height));
			setPluginConfig((SycamorePlugin) _comboInitialConditions.getSelectedItem(), getButtonConfigureInitialConditions());
			_comboInitialConditions.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					setPluginConfig((SycamorePlugin) _comboInitialConditions.getSelectedItem(), getButtonConfigureInitialConditions());
				}
			});
		}
		return _comboInitialConditions;
	}

	public InitialConditions<?> getSelectedInitialConditions() {

		return (InitialConditions<?>) getComboInitialConditions().getSelectedItem();
	}

	/**
	 * @return the _comboScheduler
	 */
	public JComboBox<Scheduler<?>> getComboScheduler() {

		if (_comboScheduler == null) {
			_comboScheduler = new JComboBox<>(getPluginManager().getLoadedSchedulers());
			_comboScheduler.insertItemAt(null, 0);
			_comboScheduler.setSelectedIndex(0);
			_comboScheduler.setPreferredSize(new Dimension(200, _comboScheduler.getPreferredSize().height));
			setPluginConfig((SycamorePlugin) _comboScheduler.getSelectedItem(), getButtonConfigureScheduler());
			_comboScheduler.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					setPluginConfig((SycamorePlugin) _comboScheduler.getSelectedItem(), getButtonConfigureScheduler());
				}
			});
		}
		return _comboScheduler;
	}

	public Scheduler<?> getSelectedScheduler() {

		return (Scheduler<?>) getComboScheduler().getSelectedItem();
	}

	/**
	 * @return the _comboMemory
	 */
	public JComboBox<Memory<?>> getComboMemory() {

		if (_comboMemory == null) {
			_comboMemory = new JComboBox<>(getPluginManager().getLoadedMemories(TYPE.TYPE_2D));
			_comboMemory.insertItemAt(null, 0);
			_comboMemory.setSelectedIndex(0);
			_comboMemory.setPreferredSize(new Dimension(200, _comboMemory.getPreferredSize().height));
			setPluginConfig((SycamorePlugin) _comboMemory.getSelectedItem(), getButtonConfigureMemory());
			_comboMemory.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					setPluginConfig((SycamorePlugin) _comboMemory.getSelectedItem(), getButtonConfigureMemory());
				}
			});
		}
		return _comboMemory;
	}

	public Memory<?> getSelectedMemory() {

		return (Memory<?>) getComboMemory().getSelectedItem();
	}

	/**
	 * @return the _comboAgreement
	 */
	public JComboBox<Agreement<?>> getComboAgreement() {

		if (_comboAgreement == null) {
			_comboAgreement = new JComboBox<>(getPluginManager().getLoadedAgreements(TYPE.TYPE_2D));
			_comboAgreement.insertItemAt(null, 0);
			_comboAgreement.setSelectedIndex(0);
			_comboAgreement.setPreferredSize(new Dimension(200, _comboAgreement.getPreferredSize().height));
			setPluginConfig((SycamorePlugin) _comboAgreement.getSelectedItem(), getButtonConfigureAgreement());
			_comboAgreement.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					setPluginConfig((SycamorePlugin) _comboAgreement.getSelectedItem(), getButtonConfigureAgreement());
				}
			});
		}
		return _comboAgreement;
	}

	public Agreement<?> getSelectedAgreement() {

		return (Agreement<?>) getComboAgreement().getSelectedItem();
	}

	private void setPluginConfig(SycamorePlugin plugin, JButton configButton) {

		if (configButton == null)
			return;
		else if (plugin == null)
			configButton.setEnabled(false);
		else
			configButton.setEnabled(plugin.getPanel_settings() != null);
	}

	public void setComponentsEnabled(boolean enabled) {

		getComboVisibility().setEnabled(enabled);
		getButtonConfigureVisibility().setEnabled(enabled);

		getComboInitialConditions().setEnabled(enabled);
		getButtonConfigureInitialConditions().setEnabled(enabled);

		getComboScheduler().setEnabled(enabled);
		getButtonConfigureScheduler().setEnabled(enabled);

		getComboMemory().setEnabled(enabled);
		getButtonConfigureMemory().setEnabled(enabled);

		getComboAgreement().setEnabled(enabled);
		getButtonConfigureAgreement().setEnabled(enabled);

		getTextNumberOfSimulations().setEnabled(enabled);
	}

	/**
	 * @return the _pluginManager
	 */
	private SycamoreBatchPluginManager getPluginManager() {

		return _pluginManager;
	}

	/**
	 * @return the _listeners
	 */
	private ArrayList<ActionListener> getListeners() {

		if (_listeners == null) {
			this._listeners = new ArrayList<>();
		}

		return _listeners;
	}

	public void addActionListener(ActionListener listener) {

		getListeners().add(listener);
	}

}
