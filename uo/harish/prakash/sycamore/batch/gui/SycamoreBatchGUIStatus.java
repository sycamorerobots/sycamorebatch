/**
 * 
 */
package uo.harish.prakash.sycamore.batch.gui;

/**
 * @author harry
 *
 */
public enum SycamoreBatchGUIStatus
{
	START_SIMULATION,
	MANAGE_ROBOTS,
	MANAGE_MEASURES,
	CONFIGURE_PLUGIN
}
