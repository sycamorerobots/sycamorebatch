/**
 * 
 */
package uo.harish.prakash.sycamore.batch.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 * @author harry
 *
 */
public class SycamoreBatchDialogBox extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean _oneTimeUse;

	private JButton _buttonDone;

	private JDialog						_self;
	private Window						_parent;
	private SycamoreBatchDialogPanel	_content;
	private JPanel						_contentSimple;

	public SycamoreBatchDialogBox(Window parent, SycamoreBatchDialogPanel content, String title) {

		this(parent, content, title, true);
	}

	public SycamoreBatchDialogBox(Window parent, JPanel content, String title) {

		this(parent, content, title, true);
	}

	public SycamoreBatchDialogBox(Window parent, SycamoreBatchDialogPanel content, String title, boolean oneTimeUse) {

		super(parent, title, java.awt.Dialog.ModalityType.APPLICATION_MODAL);

		if (content == null) {
			return;
		}
		content.setParentWindow(this);

		this._content = content;
		Initialize(parent, oneTimeUse);
	}

	public SycamoreBatchDialogBox(Window parent, JPanel content, String title, boolean OneTimeUse) {

		super(parent, title, java.awt.Dialog.ModalityType.APPLICATION_MODAL);

		if (content == null) {
			return;
		}

		this._contentSimple = content;
		Initialize(parent, OneTimeUse);
	}

	private void Initialize(Window parent, boolean oneTimeUse) {

		this._self = this;
		this._parent = parent;
		this._oneTimeUse = oneTimeUse;

		setupGUI();
		setVisible(oneTimeUse);
	}

	/**
	 * @return the _oneTimeUse
	 */
	private boolean isOneTimeUse() {

		return _oneTimeUse;
	}

	/**
	 * @return the _buttonDone
	 */
	private JButton getButtonDone() {

		if (_buttonDone == null) {
			_buttonDone = new JButton("Done");
			_buttonDone.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					getSelf().setVisible(false);
					if (isOneTimeUse()) {
						getSelf().dispose();
					}
				}
			});
		}
		return _buttonDone;
	}

	/**
	 * @return the _self
	 */
	private JDialog getSelf() {

		return _self;
	}

	/**
	 * @return the _parent
	 */
	private Window getParentWindow() {

		return _parent;
	}

	/**
	 * @return the _content
	 */
	private JPanel getContent() {

		if (_content == null) {
			return _contentSimple;
		}
		else {
			return _content;
		}
	}

	private void setupGUI() {

		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints c;
		layout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };

		setLayout(layout);
		setLocationRelativeTo(getParentWindow());

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.insets = new Insets(15, 10, 10, 10);
		add(getContent(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.PAGE_START;
		c.insets = new Insets(0, 10, 15, 10);
		add(getButtonDone(), c);

		pack();
		setLocation(
			getParentWindow().getWidth() / 2 - getSelf().getWidth() / 2,
			getParentWindow().getHeight() / 2 - getSelf().getHeight() / 2);
	}

}
