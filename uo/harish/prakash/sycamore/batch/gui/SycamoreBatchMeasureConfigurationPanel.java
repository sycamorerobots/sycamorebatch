/**
 * 
 */
package uo.harish.prakash.sycamore.batch.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import it.diunipi.volpi.sycamore.plugins.SycamorePlugin;
import it.diunipi.volpi.sycamore.plugins.measures.Measure;

/**
 * @author harry
 *
 */
public class SycamoreBatchMeasureConfigurationPanel extends SycamoreBatchDialogPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Measure[]							_allMeasures;
	private ArrayList<SycamoreBatchMeasureRow>	_sycamoreBatchMeasureRows;

	private JButton	_buttonAddNewRow;
	private JLabel	_labelMeasure;

	public SycamoreBatchMeasureConfigurationPanel(Measure[] measures) {

		this._allMeasures = measures;
		setBorder(BorderFactory.createTitledBorder("Measures"));
		addNewMeasureRow();
		setPreferredSize(new Dimension(400, 400));
	}

	/**
	 * @return the _measures
	 */
	private Measure[] getAllMeasures() {

		return _allMeasures;
	}

	/**
	 * @return the _selectedMeasures
	 */
	private ArrayList<SycamoreBatchMeasureRow> getSycamoreBatchMeasureRows() {

		if (_sycamoreBatchMeasureRows == null) {
			_sycamoreBatchMeasureRows = new ArrayList<>();
		}

		return _sycamoreBatchMeasureRows;
	}

	/**
	 * @return the _buttonAddNewRow
	 */
	private JButton getButtonAddNewRow() {

		if (_buttonAddNewRow == null) {
			_buttonAddNewRow = new JButton("+");
			_buttonAddNewRow.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					addNewMeasureRow();

				}
			});
		}

		return _buttonAddNewRow;
	}

	/**
	 * @return the _labelMeasure
	 */
	private JLabel getLabelMeasure() {

		if (_labelMeasure == null) {
			_labelMeasure = new JLabel("Measure");
		}
		return _labelMeasure;
	}

	public void addNewMeasureRow() {

		if (getSycamoreBatchMeasureRows().size() > 4) {
			JOptionPane.showMessageDialog(
				getParentWindow(),
				"The number of measuers is limited to 5",
				"Note",
				JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		SycamoreBatchMeasureRow row = new SycamoreBatchMeasureRow(getAllMeasures(), getParentWindow());
		row.getButtonRemoveRow().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				getSycamoreBatchMeasureRows().remove(row);
				refreshWindow();
			}

		});

		getSycamoreBatchMeasureRows().add(row);
		refreshWindow();
	}

	private void refreshWindow() {

		removeAll();

		GridBagLayout layout = new GridBagLayout();
		layout.columnWeights = new double[] { 0.01, 0.8, 0.1 };
		layout.rowWeights = new double[getSycamoreBatchMeasureRows().size() + 3];

		for (int index = 0; index < layout.rowWeights.length; index++) {
			layout.rowWeights[index] = Double.MIN_VALUE;
		}

		layout.rowWeights[layout.rowWeights.length - 1] = 1;
		setLayout(layout);

		GridBagConstraints c;
		int currentRow = 0;

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = currentRow;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.insets = new Insets(10, 10, 5, 10);
		add(getButtonAddNewRow(), c);

		if (getSycamoreBatchMeasureRows().size() > 0) {

			currentRow = currentRow + 1;
			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = currentRow;
			c.insets = new Insets(5, 10, 5, 10);
			add(getLabelMeasure(), c);
		}

		for (SycamoreBatchMeasureRow row : getSycamoreBatchMeasureRows()) {

			currentRow = currentRow + 1;

			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = currentRow;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(5, 10, 5, 10);
			add(row.getButtonConfigurePlugin(), c);

			c = new GridBagConstraints();
			c.gridx = 1;
			c.gridy = currentRow;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(5, 10, 5, 10);
			add(row.getComboBoxMeasures(), c);

			c = new GridBagConstraints();
			c.gridx = 2;
			c.gridy = currentRow;
			c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(5, 10, 5, 10);
			add(row.getButtonRemoveRow(), c);
		}

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = layout.rowWeights.length - 1;
		c.gridwidth = 3;
		c.fill = GridBagConstraints.BOTH;
		add(new javax.swing.JPanel(), c);

		revalidate();
		repaint();
	}

	public ArrayList<Measure> getSelectedMeasures() {

		ArrayList<Measure> selectedMeasures = new ArrayList<>();
		for (SycamoreBatchMeasureRow row : getSycamoreBatchMeasureRows()) {
			try {
				Measure measure = Measure.class.cast(row.getSelectedMeasure());
				selectedMeasures.add(measure);
			}
			catch (ClassCastException ex) {}
		}

		return selectedMeasures;
	}

	public void setComponentsEnabled(boolean enabled) {

		getButtonAddNewRow().setEnabled(enabled);
		for (SycamoreBatchMeasureRow row : getSycamoreBatchMeasureRows()) {
			row.setEnabled(enabled);
		}
	}

}

class SycamoreBatchMeasureRow {

	private JButton				_buttonConfigurePlugin;
	private JButton				_buttonRemoveRow;
	private JComboBox<Measure>	_comboBoxMeasures;
	private Measure[]			_availableMeasures;

	private java.awt.Window _parent;

	public SycamoreBatchMeasureRow(Measure[] measures, java.awt.Window parent) {

		this._availableMeasures = measures;
		this._parent = parent;
		SetConfigButton();
	}

	/**
	 * @return the _buttonConfigurePlugin
	 */
	public JButton getButtonConfigurePlugin() {

		if (_buttonConfigurePlugin == null) {
			_buttonConfigurePlugin = new JButton("...");
			_buttonConfigurePlugin.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					if (getSelectedMeasure() != null) {
						new SycamoreBatchDialogBox(getParent(), getSelectedMeasure().getPanel_settings(), getSelectedMeasure().getPluginName());
					}

				}
			});
		}
		return _buttonConfigurePlugin;
	}

	/**
	 * @return the _buttonRemoveRow
	 */
	public JButton getButtonRemoveRow() {

		if (_buttonRemoveRow == null) {
			_buttonRemoveRow = new JButton("-");
		}

		return _buttonRemoveRow;
	}

	/**
	 * @return the _comboBoxMeasures
	 */
	public JComboBox<Measure> getComboBoxMeasures() {

		if (_comboBoxMeasures == null) {
			_comboBoxMeasures = new JComboBox<>(getAvailableMeasures());

			_comboBoxMeasures.insertItemAt(null, 0);
			_comboBoxMeasures.setSelectedIndex(0);

			_comboBoxMeasures.setPreferredSize(new Dimension(120, _comboBoxMeasures.getPreferredSize().height));
			_comboBoxMeasures.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					SetConfigButton();

				}
			});
		}
		return _comboBoxMeasures;
	}

	public SycamorePlugin getSelectedMeasure() {

		SycamorePlugin selected = null;

		if (getComboBoxMeasures().getSelectedItem() == null) {
			return selected;
		}

		if (SycamorePlugin.class.isAssignableFrom(getComboBoxMeasures().getSelectedItem().getClass())) {
			selected = SycamorePlugin.class.cast(getComboBoxMeasures().getSelectedItem());
		}

		return selected;
	}

	/**
	 * @return the _availableMeasures
	 */
	private Measure[] getAvailableMeasures() {

		return _availableMeasures;
	}

	/**
	 * @return the _parent
	 */
	private java.awt.Window getParent() {

		return _parent;
	}

	private void SetConfigButton() {

		if (getSelectedMeasure() != null) {
			getButtonConfigurePlugin().setEnabled(getSelectedMeasure().getPanel_settings() != null);
		}

		else {
			getButtonConfigurePlugin().setEnabled(false);
		}
	}

	public void setEnabled(boolean enabled) {

		getButtonConfigurePlugin().setEnabled(enabled);
		getButtonRemoveRow().setEnabled(enabled);
		getComboBoxMeasures().setEnabled(enabled);
	}
}
