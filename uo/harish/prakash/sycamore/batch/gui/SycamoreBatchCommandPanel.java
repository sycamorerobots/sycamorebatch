/**
 * 
 */
package uo.harish.prakash.sycamore.batch.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * @author harry
 *
 */
public class SycamoreBatchCommandPanel extends JPanel {

	/**
	 * 
	 */
	private static final long			serialVersionUID	= 1L;
	private ArrayList<ActionListener>	_listeners;

	private JButton	_startSimulation;
	private JButton	_manageRobots;
	private JButton	_manageMeasures;

	public SycamoreBatchCommandPanel() {
		setupGUI();
	}

	/**
	 * @return the _listeners
	 */
	public ArrayList<ActionListener> getListeners() {

		if (_listeners == null) {
			_listeners = new ArrayList<>();
		}
		return _listeners;
	}

	/**
	 * @return the _startSimulation
	 */
	public JButton getStartSimulation() {

		if (_startSimulation == null) {
			_startSimulation = new JButton("Start Simulation");
			_startSimulation.setMaximumSize(new Dimension(250, 30));
			_startSimulation.setAlignmentX(CENTER_ALIGNMENT);
			_startSimulation.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					fireEvent(new ActionEvent(System.currentTimeMillis(), 0, SycamoreBatchGUIStatus.START_SIMULATION.name()));
				}
			});
		}
		return _startSimulation;
	}

	/**
	 * @return the _manageRobots
	 */
	public JButton getManageRobots() {

		if (_manageRobots == null) {
			_manageRobots = new JButton("Manage Robots");
			_manageRobots.setMaximumSize(new Dimension(250, 30));
			_manageRobots.setAlignmentX(CENTER_ALIGNMENT);
			_manageRobots.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					fireEvent(new ActionEvent(System.currentTimeMillis(), 0, SycamoreBatchGUIStatus.MANAGE_ROBOTS.name()));
				}
			});
		}
		return _manageRobots;
	}

	/**
	 * @return the _manageMeasures
	 */
	public JButton getManageMeasures() {

		if (_manageMeasures == null) {
			_manageMeasures = new JButton("Manage Measures");
			_manageMeasures.setMaximumSize(new Dimension(250, 30));
			_manageMeasures.setAlignmentX(CENTER_ALIGNMENT);
			_manageMeasures.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					fireEvent(new ActionEvent(System.currentTimeMillis(), 0, SycamoreBatchGUIStatus.MANAGE_MEASURES.name()));
				}
			});
		}
		return _manageMeasures;
	}

	public void setComponentsEnabled(boolean enable) {

		getStartSimulation().setEnabled(enable);
	}

	public void addActionListener(ActionListener listener) {

		getListeners().add(listener);
	}

	private void fireEvent(ActionEvent event) {

		for (ActionListener listener : getListeners()) {
			listener.actionPerformed(event);
		}
	}

	private void setupGUI() {

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		add(Box.createRigidArea(new Dimension(250, 10)));
		add(getManageRobots());
		add(Box.createRigidArea(new Dimension(250, 10)));
		add(getManageMeasures());
		add(Box.createRigidArea(new Dimension(250, 10)));
		add(getStartSimulation());

	}
}
