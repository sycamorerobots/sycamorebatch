/**
 * 
 */
package uo.harish.prakash.sycamore.batch.gui;

import java.awt.Window;
import javax.swing.JPanel;

/**
 * @author harry
 *
 */
public abstract class SycamoreBatchDialogPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Window _parent;

	/**
	 * @return the _parentWindow
	 */
	public Window getParentWindow() {

		return _parent;
	}

	/**
	 * @param parent
	 *            the _parentWindow to set
	 */
	public void setParentWindow(Window parent) {

		this._parent = parent;
	}
}
