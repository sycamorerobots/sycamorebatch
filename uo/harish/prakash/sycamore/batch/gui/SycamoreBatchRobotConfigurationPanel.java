/**
 * 
 */
package uo.harish.prakash.sycamore.batch.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import com.jme3.math.ColorRGBA;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.plugins.SycamorePlugin;
import it.diunipi.volpi.sycamore.plugins.algorithms.Algorithm;
import uo.harish.prakash.sycamore.batch.data.SycamoreBatchRobotListConfiguration;
import uo.harish.prakash.sycamore.batch.utilities.SycamoreBatchPluginManager;

/**
 * @author harry
 *
 */
public class SycamoreBatchRobotConfigurationPanel extends SycamoreBatchDialogPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<SycamoreBatchRobotConfigurationRow>	configurations;
	private SycamoreBatchPluginManager						pluginManager;
	private JButton											_buttonAddRow;
	private JLabel											_labelAlgorithm;
	private JLabel											_labelColor;
	private JLabel											_labelSpeed;
	private JLabel											_labelCount;

	public SycamoreBatchRobotConfigurationPanel(SycamoreBatchPluginManager pluginManager) {
		this.configurations = new ArrayList<>();
		this.pluginManager = pluginManager;
		setupGUI();
		addConfigurationRow();
	}

	/**
	 * @return the _buttonAddRow
	 */
	public JButton getButtonAddRow() {

		if (_buttonAddRow == null) {
			_buttonAddRow = new JButton("+");
			_buttonAddRow.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					addConfigurationRow();
				}

			});
		}
		return _buttonAddRow;
	}

	/**
	 * @return the _labelAlgorithm
	 */
	private JLabel getLabelAlgorithm() {

		if (_labelAlgorithm == null) {
			_labelAlgorithm = new JLabel("Algorithm");
		}

		return _labelAlgorithm;
	}

	/**
	 * @return the labelColor
	 */
	private JLabel getLabelColor() {

		if (_labelColor == null) {
			_labelColor = new JLabel("Color");
		}
		return _labelColor;
	}

	/**
	 * @return the _labelSpeed
	 */
	private JLabel getLabelSpeed() {

		if (_labelSpeed == null) {
			_labelSpeed = new JLabel("Speed");
		}

		return _labelSpeed;
	}

	/**
	 * @return the _labelCount
	 */
	private JLabel getLabelCount() {

		if (_labelCount == null) {
			_labelCount = new JLabel("Count");
		}

		return _labelCount;
	}

	private void setupGUI() {

		setPreferredSize(new Dimension(640, 480));
		setBorder(BorderFactory.createTitledBorder("Robot Configuration"));
	}

	private void addConfigurationRow() {

		if (configurations.size() > 4) {
			JOptionPane.showMessageDialog(this, "Cannot add more than 5 robot lists", "Note", JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		SycamoreBatchRobotConfigurationRow row = new SycamoreBatchRobotConfigurationRow(pluginManager, getParentWindow());
		row.getButtonRemoveRow().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				configurations.remove(row);
				refreshPanel();
			}
		});

		configurations.add(row);
		refreshPanel();

	}

	private void refreshPanel() {

		GridBagLayout layout = new GridBagLayout();
		layout.rowWeights = new double[configurations.size() + 3];

		for (int index = 0; index < layout.rowWeights.length; index++) {
			layout.rowWeights[index] = Double.MIN_VALUE;
		}

		layout.columnWeights = new double[] { 0.05, 0.3, 0.3, 0.15, 0.15, 0.05 };
		layout.rowWeights[layout.rowWeights.length - 1] = 1;
		setLayout(layout);

		GridBagConstraints c;
		removeAll();

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		add(getButtonAddRow(), c);

		Iterator<SycamoreBatchRobotConfigurationRow> rows = configurations.iterator();
		int currentRow = 0;

		if (configurations.size() > 0) {

			currentRow = currentRow + 1;

			c = new GridBagConstraints();
			c.gridx = 1;
			c.gridy = currentRow;
			add(getLabelAlgorithm(), c);

			c = new GridBagConstraints();
			c.gridx = 2;
			c.gridy = currentRow;
			add(getLabelColor(), c);

			c = new GridBagConstraints();
			c.gridx = 3;
			c.gridy = currentRow;
			add(getLabelSpeed(), c);

			c = new GridBagConstraints();
			c.gridx = 4;
			c.gridy = currentRow;
			add(getLabelCount(), c);
		}

		while (rows.hasNext()) {

			currentRow = currentRow + 1;
			SycamoreBatchRobotConfigurationRow row = rows.next();

			c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = currentRow;
			c.insets = new Insets(10, 0, 0, 5);
			c.fill = GridBagConstraints.BOTH;
			add(row.getButtonPluginConfiguration(), c);

			c = new GridBagConstraints();
			c.gridx = 1;
			c.gridy = currentRow;
			c.insets = new Insets(10, 0, 0, 5);
			c.fill = GridBagConstraints.BOTH;
			add(row.getComboAlgorithm(), c);

			c = new GridBagConstraints();
			c.gridx = 2;
			c.gridy = currentRow;
			c.insets = new Insets(10, 0, 0, 5);
			c.fill = GridBagConstraints.BOTH;
			add(row.getComboColor(), c);

			c = new GridBagConstraints();
			c.gridx = 3;
			c.gridy = currentRow;
			c.insets = new Insets(10, 0, 0, 5);
			c.fill = GridBagConstraints.BOTH;
			add(row.getTextSpeed(), c);

			c = new GridBagConstraints();
			c.gridx = 4;
			c.gridy = currentRow;
			c.insets = new Insets(10, 0, 0, 5);
			c.fill = GridBagConstraints.BOTH;
			add(row.getTextCount(), c);

			c = new GridBagConstraints();
			c.gridx = 5;
			c.gridy = currentRow;
			c.insets = new Insets(10, 0, 0, 5);
			c.fill = GridBagConstraints.BOTH;
			add(row.getButtonRemoveRow(), c);
		}

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = currentRow + 1;
		c.fill = GridBagConstraints.BOTH;
		add(new JPanel(), c);

		repaint();
		revalidate();
	}

	public ArrayList<SycamoreBatchRobotListConfiguration> getSycamoreBatchRobotListConfigurations() {

		ArrayList<SycamoreBatchRobotListConfiguration> configurations = new ArrayList<>();
		for (SycamoreBatchRobotConfigurationRow row : this.configurations) {
			configurations.add(row.getSycamoreBatchRobotListConfiguration());
		}

		return configurations;
	}

	public void setComponentsEnabled(boolean enabled) {

		getButtonAddRow().setEnabled(enabled);
		for (SycamoreBatchRobotConfigurationRow row : this.configurations) {
			row.setRowEnabled(enabled);
		}

	}

}

class SycamoreBatchRobotConfigurationRow {

	private JComboBox<Algorithm<?>>		_comboAlgorithm;
	private JTextField					_textSpeed;
	private JTextField					_textCount;
	private JButton						_buttonRemoveRow;
	private JButton						_buttonPluginConfiguration;
	private JComboBox<ColorRGBA>		_comboBoxColor;
	private SycamoreBatchPluginManager	_pluginManager;
	private java.awt.Window				_parentWindow;

	public SycamoreBatchRobotConfigurationRow(SycamoreBatchPluginManager pluginManager, java.awt.Window parentWindow) {

		this._pluginManager = pluginManager;
		this._parentWindow = parentWindow;
		SetConfigButton();
	}

	/**
	 * @return the _comboAlgorithm
	 */
	public JComboBox<Algorithm<?>> getComboAlgorithm() {

		if (_comboAlgorithm == null) {
			_comboAlgorithm = new JComboBox<>(_pluginManager.getLoadedAlgorithms(TYPE.TYPE_2D));

			_comboAlgorithm.insertItemAt(null, 0);
			_comboAlgorithm.setSelectedIndex(0);

			_comboAlgorithm.setPreferredSize(new Dimension(120, _comboAlgorithm.getPreferredSize().height));
			_comboAlgorithm.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					SetConfigButton();
				}
			});
		}

		return _comboAlgorithm;
	}

	/**
	 * @return the _textSpeed
	 */
	public JTextField getTextSpeed() {

		if (_textSpeed == null) {
			_textSpeed = new JTextField("1");
		}

		return _textSpeed;
	}

	/**
	 * @return the _textCount
	 */
	public JTextField getTextCount() {

		if (_textCount == null) {
			_textCount = new JTextField("1");
		}

		return _textCount;
	}

	/**
	 * @return the _buttonRemoveRow
	 */
	public JButton getButtonRemoveRow() {

		if (_buttonRemoveRow == null) {

			_buttonRemoveRow = new JButton("-");
		}

		return _buttonRemoveRow;
	}

	/**
	 * @return the _buttonPluginConfiguration
	 */
	public JButton getButtonPluginConfiguration() {

		if (_buttonPluginConfiguration == null) {

			_buttonPluginConfiguration = new JButton("...");
			_buttonPluginConfiguration.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					if (getComboAlgorithm().getSelectedItem() == null) {
						return;
					}
					if (!SycamorePlugin.class.isAssignableFrom(getComboAlgorithm().getSelectedItem().getClass())) {
						return;
					}

					SycamorePlugin plugin = SycamorePlugin.class.cast(getComboAlgorithm().getSelectedItem());
					if (plugin.getPanel_settings() == null)
						return;

					new SycamoreBatchDialogBox(getParentWindow(), plugin.getPanel_settings(), plugin.getPluginName());
				}
			});
		}

		return _buttonPluginConfiguration;
	}

	/**
	 * @return the comboBoxColor
	 */
	public JComboBox<ColorRGBA> getComboColor() {

		if (_comboBoxColor == null) {
			ColorRGBA[] colors = new ColorRGBA[12];

			colors[0] = (ColorRGBA.Black);
			colors[1] = (ColorRGBA.Blue);
			colors[2] = (ColorRGBA.Brown);
			colors[3] = (ColorRGBA.Cyan);
			colors[4] = (ColorRGBA.Gray);
			colors[5] = (ColorRGBA.Green);
			colors[6] = (ColorRGBA.Magenta);
			colors[7] = (ColorRGBA.Orange);
			colors[8] = (ColorRGBA.Pink);
			colors[9] = (ColorRGBA.Red);
			colors[10] = (ColorRGBA.White);
			colors[11] = (ColorRGBA.Yellow);

			_comboBoxColor = new JComboBox<>(colors);
			_comboBoxColor.setPreferredSize(new Dimension(80, _comboAlgorithm.getPreferredSize().height));
			_comboBoxColor.setRenderer(SycamoreColorCellRenderer.getInstance());
		}

		return _comboBoxColor;
	}

	/**
	 * @return the _parentWindow
	 */
	private java.awt.Window getParentWindow() {

		return _parentWindow;
	}

	private void SetConfigButton() {

		SycamorePlugin algorithm = (SycamorePlugin) getComboAlgorithm().getSelectedItem();
		if (algorithm != null && algorithm.getPanel_settings() != null) {
			getButtonPluginConfiguration().setEnabled(true);
		}
		else {
			getButtonPluginConfiguration().setEnabled(false);
		}
	}

	@SuppressWarnings("unchecked")
	public SycamoreBatchRobotListConfiguration getSycamoreBatchRobotListConfiguration() {

		SycamoreBatchRobotListConfiguration config = new SycamoreBatchRobotListConfiguration();
		config.setAlgorithm((Algorithm<Point2D>) getComboAlgorithm().getSelectedItem());

		int count = 0;
		float speed = 1f;

		// @formatter:off
		try { count = Integer.parseInt(getTextCount().getText()); }
		catch (NumberFormatException ex) {}
		try { speed = Float.parseFloat(getTextSpeed().getText()); }
		catch (NumberFormatException ex) {}
		// @formatter:on

		config.setSpeed(speed);
		config.setColor((ColorRGBA) getComboColor().getSelectedItem());
		config.setNumberOfRobots(count);

		return config;
	}

	public void setRowEnabled(boolean enabled) {

		getButtonRemoveRow().setEnabled(enabled);
		getButtonPluginConfiguration().setEnabled(enabled);
		getComboAlgorithm().setEnabled(enabled);
		getComboColor().setEnabled(enabled);
		getTextCount().setEnabled(enabled);
		getTextSpeed().setEnabled(enabled);

	}
}

class SycamoreColorCellRenderer extends DefaultListCellRenderer {

	private static final long serialVersionUID = 1L;

	private SycamoreColorCellRenderer() {}

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

		Component component = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

		if (value == null) {
			return component;
		}
		else if (!JLabel.class.isAssignableFrom(component.getClass())) {
			return component;
		}
		else if (!ColorRGBA.class.isAssignableFrom(value.getClass())) {
			return component;
		}

		ColorRGBA selected = ColorRGBA.class.cast(value);
		JLabel label = JLabel.class.cast(component);

		if (selected.equals(ColorRGBA.Black)) {
			label.setText("Black");
		}
		if (selected.equals(ColorRGBA.Blue)) {
			label.setText("Blue");
		}
		if (selected.equals(ColorRGBA.Brown)) {
			label.setText("Brown");
		}
		if (selected.equals(ColorRGBA.Cyan)) {
			label.setText("Cyan");
		}
		if (selected.equals(ColorRGBA.Gray)) {
			label.setText("Gray");
		}
		if (selected.equals(ColorRGBA.Green)) {
			label.setText("Green");
		}
		if (selected.equals(ColorRGBA.Magenta)) {
			label.setText("Magenta");
		}
		if (selected.equals(ColorRGBA.Orange)) {
			label.setText("Orange");
		}
		if (selected.equals(ColorRGBA.Pink)) {
			label.setText("Pink");
		}
		if (selected.equals(ColorRGBA.Red)) {
			label.setText("Red");
		}
		if (selected.equals(ColorRGBA.White)) {
			label.setText("White");
		}
		if (selected.equals(ColorRGBA.Yellow)) {
			label.setText("Yellow");
		}

		return label;
	}

	public static SycamoreColorCellRenderer getInstance() {

		return new SycamoreColorCellRenderer();
	}

}