/**
 * 
 */
package uo.harish.prakash.sycamore.batch.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.*;
import uo.harish.prakash.sycamore.batch.core.SycamoreBatch;
import uo.harish.prakash.sycamore.batch.core.SycamoreBatchEvent;

/**
 * @author harry
 *
 */
public class SycamoreBatchConsolePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private SycamoreBatch	_batchSimulator;
	private JTextPane		_consoleText;
	private JScrollPane		_consoleScroll;

	public SycamoreBatchConsolePanel(SycamoreBatch BatchSimulator) {
		this._batchSimulator = BatchSimulator;
		setupGUI();
		setupBatchEvents();
	}

	/**
	 * @return the _batchSimulator
	 */
	public SycamoreBatch getBatchSimulator() {

		return _batchSimulator;
	}

	/**
	 * @return the _consoleText
	 */
	public JTextPane getConsoleText() {

		if (_consoleText == null) {
			_consoleText = new JTextPane();
			_consoleText.setEditable(false);
			_consoleText.setBackground(java.awt.Color.BLACK);
		}
		return _consoleText;
	}

	/**
	 * @return the _consoleScroll
	 */
	public JScrollPane getConsoleScroll() {

		if (_consoleScroll == null) {
			_consoleScroll = new JScrollPane(getConsoleText());
		}
		return _consoleScroll;
	}

	public void println(String Message) {

		String now = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date()).toString();
		Style whiteFG = getConsoleText().getStyle("WhiteForeground");
		if (whiteFG == null) {
			whiteFG = getConsoleText().addStyle("WhiteForeground", getConsoleText().getStyle(StyleContext.DEFAULT_STYLE));
			StyleConstants.setForeground(whiteFG, java.awt.Color.WHITE);
		}
		StyledDocument document = getConsoleText().getStyledDocument();
		//@formatter:off
		try {document.insertString(document.getLength(), String.format("%s| %s\n", now, Message), whiteFG);}
		catch (BadLocationException e) {}
		//@formatter:on
	}

	private void setupGUI() {

		GridBagLayout layout = new GridBagLayout();

		layout.rowWeights = new double[] { 1.0 };
		layout.columnWeights = new double[] { 1.0 };

		setLayout(layout);

		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.fill = GridBagConstraints.BOTH;
		add(getConsoleScroll(), c);

		println("SycamoreBatch ready...");
	}

	private void setupBatchEvents() {

		getBatchSimulator().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (e.getActionCommand().equals(SycamoreBatchEvent.SIMULATION_FINISHED.name())) {
					println(String.format("Completed simulation: %d", e.getID()));
				}

				else if (e.getActionCommand().equals(SycamoreBatchEvent.BATCH_FINISHED.name())) {
					println("The batch has completed, please close the window and open it again to perform another batch");
				}

			}
		});
	}

}
