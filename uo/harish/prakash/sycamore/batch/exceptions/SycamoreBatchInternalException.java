/**
 * 
 */
package uo.harish.prakash.sycamore.batch.exceptions;

/**
 * @author harry
 *
 */
public class SycamoreBatchInternalException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SycamoreBatchInternalException(String message) {
		super(message);
	}

}
