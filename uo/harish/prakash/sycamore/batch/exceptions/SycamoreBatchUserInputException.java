/**
 * 
 */
package uo.harish.prakash.sycamore.batch.exceptions;

/**
 * @author harry
 *
 */
public class SycamoreBatchUserInputException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SycamoreBatchUserInputException(String message) {
		super(message);
	}

}
