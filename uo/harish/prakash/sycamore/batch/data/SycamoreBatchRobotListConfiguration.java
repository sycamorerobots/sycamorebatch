/**
 * 
 */
package uo.harish.prakash.sycamore.batch.data;

import com.jme3.math.ColorRGBA;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.plugins.algorithms.Algorithm;

/**
 * @author harry
 *
 */

public class SycamoreBatchRobotListConfiguration {

	private int					_numberOfRobots;
	private ColorRGBA			_color;
	private Algorithm<Point2D>	_algorithm;
	private float				_speed;
	private int					_numberOfLights;

	/**
	 * @return the _numberOfRobots
	 */
	public int getNumberOfRobots() {

		return _numberOfRobots;
	}

	/**
	 * @param _numberOfRobots
	 *            the _numberOfRobots to set
	 */
	public void setNumberOfRobots(int _numberOfRobots) {

		this._numberOfRobots = _numberOfRobots;
	}

	/**
	 * @return the _color
	 */
	public ColorRGBA getColor() {

		if (_color == null)
			_color = ColorRGBA.Black;
		return _color;
	}

	/**
	 * @param _color
	 *            the _color to set
	 */
	public void setColor(ColorRGBA _color) {

		this._color = _color;
	}

	/**
	 * @return the _algorithm
	 */
	public Algorithm<Point2D> getAlgorithm() {

		return _algorithm;
	}

	/**
	 * @param _algorithm
	 *            the _algorithm to set
	 */
	public void setAlgorithm(Algorithm<Point2D> _algorithm) {

		this._algorithm = _algorithm;
	}

	/**
	 * @return the _speed
	 */
	public float getSpeed() {

		if (_speed < 0.1)
			_speed = 0.1f;
		return _speed;
	}

	/**
	 * @param _speed
	 *            the _speed to set
	 */
	public void setSpeed(float _speed) {

		this._speed = _speed;
	}

	/**
	 * @return the _numberOfLights
	 */
	public int getNumberOfLights() {

		return _numberOfLights;
	}

	/**
	 * @param _numberOfLights
	 *            the _numberOfLights to set
	 */
	public void setNumberOfLights(int _numberOfLights) {

		this._numberOfLights = _numberOfLights;
	}
}