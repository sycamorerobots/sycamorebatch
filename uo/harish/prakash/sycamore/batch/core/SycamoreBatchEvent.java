/**
 * 
 */
package uo.harish.prakash.sycamore.batch.core;

/**
 * @author harry
 *
 */
public enum SycamoreBatchEvent
{
	SIMULATION_FINISHED(1),
	BATCH_FINISHED(2),
	SIMULATION_FAILED(3),
	BATCH_FAILED(4),
	SIMULATOR_COOLING(5);

	private int _eventID;

	SycamoreBatchEvent(int eventID) {
		this._eventID = eventID;
	}

	/**
	 * @return the eventID
	 */
	public int getEventID() {

		return _eventID;
	}
}
