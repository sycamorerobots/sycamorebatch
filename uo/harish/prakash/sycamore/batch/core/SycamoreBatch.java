/**
 * 
 */
package uo.harish.prakash.sycamore.batch.core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import it.diunipi.volpi.app.sycamore.SycamoreApp.APP_MODE;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine2D;
import it.diunipi.volpi.sycamore.gui.SycamoreSystem;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditions;
import it.diunipi.volpi.sycamore.plugins.measures.Measure;
import it.diunipi.volpi.sycamore.plugins.schedulers.Scheduler;
import it.diunipi.volpi.sycamore.plugins.visibilities.Visibility;
import it.diunipi.volpi.sycamore.util.SycamoreFiredActionEvents;
import uo.harish.prakash.sycamore.batch.data.SycamoreBatchRobotListConfiguration;

/**
 * @author harry
 *
 */
public class SycamoreBatch {

	private int												_numberOfSimulations;
	private Scheduler<Point2D>								_scheduler;
	private Visibility<Point2D>								_visibility;
	private InitialConditions<Point2D>						_initialConditions;
	private ArrayList<Measure>								_measures;
	private ArrayList<SycamoreBatchRobotListConfiguration>	_robotLists;
	private SycamoreBatchEngine								_batchEngine;

	public SycamoreBatch() {

		_batchEngine = new SycamoreBatchEngine();
		_measures = new ArrayList<>();
	}

	/**
	 * @return the _numberOfSimulations
	 */
	public int getNumberOfSimulations() {

		return _numberOfSimulations;
	}

	/**
	 * @param _numberOfSimulations
	 *            the _numberOfSimulations to set
	 */
	public void setNumberOfSimulations(int _numberOfSimulations) {

		this._numberOfSimulations = _numberOfSimulations;
	}

	/**
	 * @return the _scheduler
	 */
	public Scheduler<Point2D> getScheduler() {

		return _scheduler;
	}

	/**
	 * @param _scheduler
	 *            the _scheduler to set
	 */
	public void setScheduler(Scheduler<Point2D> _scheduler) {

		this._scheduler = _scheduler;
	}

	/**
	 * @return the _visibility
	 */
	public Visibility<Point2D> getVisibility() {

		return _visibility;
	}

	/**
	 * @param _visibility
	 *            the _visibility to set
	 */
	public void setVisibility(Visibility<Point2D> _visibility) {

		this._visibility = _visibility;
	}

	/**
	 * @return the _initialConditions
	 */
	public InitialConditions<Point2D> getInitialConditions() {

		return _initialConditions;
	}

	/**
	 * @param _initialConditions
	 *            the _initialConditions to set
	 */
	public void setInitialConditions(InitialConditions<Point2D> _initialConditions) {

		this._initialConditions = _initialConditions;
	}

	/**
	 * @return the _measures
	 */
	public ArrayList<Measure> getMeasures() {

		return _measures;
	}

	/**
	 * @param _measures
	 *            the _measures to set
	 */
	public void addMeasures(Measure measure) {

		this._measures.add(measure);
	}

	/**
	 * @return the _robotLists
	 */
	public ArrayList<SycamoreBatchRobotListConfiguration> getRobotLists() {

		return _robotLists;
	}

	/**
	 * @param robotConfiguration
	 *            The robot configuration to add
	 */
	public void addRobotConfiguration(SycamoreBatchRobotListConfiguration robotConfiguration) {

		if (robotConfiguration == null) {
			return;
		}

		else if (this._robotLists == null) {
			this._robotLists = new ArrayList<>();
		}

		this._robotLists.add(robotConfiguration);
	}

	public void addActionListener(ActionListener listener) {

		_batchEngine.addActionListener(listener);
	}

	public SycamoreBatchStatus StartBatch() {

		if (getScheduler() == null) {
			return SycamoreBatchStatus.ERR_INCOMPLETEDATA_SCHEDULER;
		}

		else if (getRobotLists() == null) {
			return SycamoreBatchStatus.ERR_INCOMPLETEDATA_ROBOTLIST;
		}
		else if (getNumberOfSimulations() < 1) {
			return SycamoreBatchStatus.ERR_INCOMPLETEDATA_SIMULATIONCOUNT;
		}

		else {

			_batchEngine.initialize(_numberOfSimulations, _scheduler, _visibility, _initialConditions, _measures, _robotLists);
			return SycamoreBatchStatus.SUCCESS;
		}
	}
}

class SycamoreBatchEngine {

	private int												_numberOfSimulations;
	private int												_count;
	private Scheduler<Point2D>								_scheduler;
	private Visibility<Point2D>								_visibility;
	private InitialConditions<Point2D>						_initialConditions;
	private ArrayList<Measure>								_measures;
	private ArrayList<SycamoreBatchRobotListConfiguration>	_robots;
	private ArrayList<ActionListener>						_listeners;

	public SycamoreBatchEngine() {
		_listeners = new ArrayList<>();
	}

	public void addActionListener(ActionListener listener) {

		_listeners.add(listener);
	}

	public void NotifyListeners(Object source, int ID, SycamoreBatchEvent event) {

		for (ActionListener listener : _listeners) {
			listener.actionPerformed(new ActionEvent(source, ID, event.name()));
		}
	}

	public void NotifyListeners(Object source, SycamoreBatchEvent event) {

		NotifyListeners(source, event.getEventID(), event);
	}

	public void initialize(
		int NumberOfSimulations,
		Scheduler<Point2D> Scheduler,
		Visibility<Point2D> RobotVisibility,
		InitialConditions<Point2D> SystemInitialConditions,
		ArrayList<Measure> SimulationMeasures,
		ArrayList<SycamoreBatchRobotListConfiguration> RobotList) {

		this._numberOfSimulations = NumberOfSimulations;
		this._count = 0;
		this._scheduler = Scheduler;
		this._visibility = RobotVisibility;
		this._initialConditions = SystemInitialConditions;
		this._measures = SimulationMeasures;
		this._robots = RobotList;

		SycamoreSystem.initialize(APP_MODE.SIMULATOR);
		SetupAndRun();
	}

	private void ResetSimulation(SycamoreEngine2D engine) {

		engine.dispose();
		SycamoreSystem.reset(APP_MODE.SIMULATOR);
	}

	private void SetupAndRun() {

		final SycamoreEngine2D engine = new SycamoreEngine2D();
		//@formatter:off
		try {engine.createAndSetNewSchedulerInstance(_scheduler);}
		catch (IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException e) {}
		try {engine.createAndSetNewInitialConditionsInstance(_initialConditions);}
		catch (IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException e3) {}
		//@formatter:on

		for (Measure measure : _measures) {
			//@formatter:off
			try {engine.createAndAddNewMeasureInstance(measure);}
			catch (IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException e1) {}
			//@formatter:on
		}

		for (int index = 0; index < _robots.size(); index++) {
			SycamoreBatchRobotListConfiguration config = _robots.get(index);

			engine.addNewRobotListElement();
			for (int robot = 0; robot < config.getNumberOfRobots(); robot++) {
				engine.createAndAddNewRobotInstance(false, index, config.getColor(), config.getNumberOfLights(), config.getSpeed());

				// Note: Engine does not allow the user to make any changes to
				// individual robot however, the way it is designed, the
				// visibility has to be reloaded after each robot is added, this
				// is a very bad practice but this cannot be modified without
				// changing Sycamore's core design.
				//@formatter:off
				try {engine.createAndSetNewVisibilityInstance(_visibility);}
				catch (IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException e2) {}
				//@formatter:on
			}
			//@formatter:off
			try {engine.createAndSetNewAlgorithmInstance(config.getAlgorithm(), index);}
			catch (IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException e) {}
			//@formatter:on
		}

		engine.setAnimationSpeedMultiplier(1000000);

		_count += 1;
		SycamoreSystem.setEngine(engine);
		SycamoreSystem.getSchedulerThread().setEngine(engine);

		engine.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (!e.getActionCommand().equals(SycamoreFiredActionEvents.SIMULATION_FINISHED.name()))
					return;
				NotifyListeners(engine, _count, SycamoreBatchEvent.SIMULATION_FINISHED);
				ResetSimulation(engine);
				if (_count < _numberOfSimulations) {
					NotifyListeners(engine, _count, SycamoreBatchEvent.SIMULATOR_COOLING);
					//@formatter:off
					try {Thread.sleep(5000);}
					catch (InterruptedException e1) {}
					//@formatter:on
					SetupAndRun();
				}
				else {
					NotifyListeners(0, SycamoreBatchEvent.BATCH_FINISHED);
				}
			}
		});

		SycamoreSystem.getSchedulerThread().play();
	}
}
