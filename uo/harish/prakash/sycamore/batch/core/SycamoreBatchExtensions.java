package uo.harish.prakash.sycamore.batch.core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine2D;
import it.diunipi.volpi.sycamore.gui.SycamoreSystem;
import it.diunipi.volpi.sycamore.gui.SycamoreSystem.TIMELINE_MODE;

public class SycamoreBatchExtensions {

	public static void SaveTimelineAfterEachSimulation(SycamoreBatch sycamoreBatch, File outputFolder, TIMELINE_MODE mode) {

		SycamoreSystem.setTimelineMode(mode);
		if (mode == TIMELINE_MODE.LIVE) {
			return;
		}

		final LocalDateTime start = LocalDateTime.now();
		sycamoreBatch.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (!e.getActionCommand().equals(SycamoreBatchEvent.SIMULATION_FINISHED.name()))
					return;

				SycamoreEngine2D engine = (SycamoreEngine2D) e.getSource();
				File outputFile = new File(outputFolder, String.format(
					"Timeline_%d%d%d_%d%d%d_%d.xml",
					start.getYear(),
					start.getMonth().getValue(),
					start.getDayOfMonth(),
					start.getHour(),
					start.getMinute(),
					start.getSecond(),
					e.getID()));
				try {
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = factory.newDocumentBuilder();
					Document document = builder.newDocument();
					Element element = document.createElement("Sycamore");
					element.setAttribute("Version", "v1.0");
					element.setAttribute("BuildNumber", "1.0.0");
					element.setAttribute("type", engine.getType().toString());

					element.appendChild(engine.encode(factory, builder, document));
					document.appendChild(element);

					TransformerFactory transformerFactory = TransformerFactory.newInstance();
					Transformer transformer = transformerFactory.newTransformer();
					DOMSource domsource = new DOMSource(document);

					StringWriter stringWriter = new StringWriter();
					StreamResult resultStream = new StreamResult(stringWriter);

					transformer.transform(domsource, resultStream);
					String xmlDocumentString = stringWriter.getBuffer().toString();

					if (outputFile.exists()) {
						outputFile.delete();
					}
					PrintWriter fileWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));
					fileWriter.println(xmlDocumentString);
					fileWriter.close();
				}
				catch (TransformerException | IOException | ParserConfigurationException ex) {}
			}
		});
	}
}
