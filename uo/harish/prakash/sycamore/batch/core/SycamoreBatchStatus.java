/**
 * 
 */
package uo.harish.prakash.sycamore.batch.core;

/**
 * @author harry
 *
 */
public enum SycamoreBatchStatus
{

	SUCCESS(1, "Your operation was successful"),
	ERR_INCOMPLETEDATA_SCHEDULER(2, "Please provide a scheduler"),
	ERR_INCOMPLETEDATA_SIMULATIONCOUNT(3, "Please provide the number of simulations"),
	ERR_INCOMPLETEDATA_ROBOTLIST(4, "Please setup robot lists for the simulation");

	private int _statusCode;
	private String _statusMessage;

	SycamoreBatchStatus(int StatusCode, String StatusMessage) {
		this._statusCode = StatusCode;
		this._statusMessage = StatusMessage;
	}

	/**
	 * @return the _statusCode
	 */
	public int getStatusCode() {

		return _statusCode;
	}

	/**
	 * @return the _statusMessage
	 */
	public String getStatusMessage() {

		return _statusMessage;
	}
}
