/**
 * 
 */
package uo.harish.prakash.sycamore.batch.utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.plugins.SycamorePlugin;
import it.diunipi.volpi.sycamore.plugins.agreements.Agreement;
import it.diunipi.volpi.sycamore.plugins.algorithms.Algorithm;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditions;
import it.diunipi.volpi.sycamore.plugins.measures.Measure;
import it.diunipi.volpi.sycamore.plugins.memory.Memory;
import it.diunipi.volpi.sycamore.plugins.schedulers.Scheduler;
import it.diunipi.volpi.sycamore.plugins.visibilities.Visibility;
import net.xeoh.plugins.base.Plugin;
import net.xeoh.plugins.base.PluginManager;
import net.xeoh.plugins.base.impl.PluginManagerFactory;
import net.xeoh.plugins.base.util.PluginManagerUtil;

/**
 * @author harry
 *
 */
public class SycamoreBatchPluginManager {

	private PluginManagerUtil				pluginManagerUtil;
	private ArrayList<Algorithm<?>>			loadedAlgorithms;
	private ArrayList<Scheduler<?>>			loadedSchedulers;
	private ArrayList<Visibility<?>>		loadedVisibilities;
	private ArrayList<InitialConditions<?>>	loadedInitialConditions;
	private ArrayList<Agreement<?>>			loadedAgreements;
	private ArrayList<Memory<?>>			loadedMemories;
	private ArrayList<Measure>				loadedMeasures;

	public SycamoreBatchPluginManager(File pluginsFolder) {
		PluginManager pluginManager = PluginManagerFactory.createPluginManager();
		pluginManager.addPluginsFrom(pluginsFolder.toURI());
		pluginManagerUtil = new PluginManagerUtil(pluginManager);
	}

	private <P extends SycamorePlugin> ArrayList<P> getPlugins(Class<P> pluginClass) {

		ArrayList<P> plugins = (ArrayList<P>) pluginManagerUtil.getPlugins(pluginClass);
		Collections.sort(plugins, new Comparator<P>() {

			@Override
			public int compare(P o1, P o2) {

				if (o1 == null && o2 == null) {
					return 0;
				}
				else if (o1 == null) {
					return -1;
				}
				else if (o2 == null) {
					return 1;
				}
				else {
					return o1.getPluginName().compareTo(o2.getPluginName());
				}
			}

		});

		return plugins;
	}

	/**
	 * @return the loadedAlgorithms
	 */
	private ArrayList<Algorithm<?>> getLoadedAlgorithms() {

		if (loadedAlgorithms == null) {
			loadedAlgorithms = new ArrayList<Algorithm<?>>();
			for (Plugin plugin : getPlugins(Algorithm.class)) {
				loadedAlgorithms.add((Algorithm<?>) plugin);
			}
		}
		return loadedAlgorithms;
	}

	/**
	 * @return the loadedAlgorithms
	 */
	public Algorithm<?>[] getLoadedAlgorithms(TYPE GraphicType) {

		ArrayList<Algorithm<?>> specificAlgorithms = new ArrayList<Algorithm<?>>();

		for (Algorithm<?> algorithm : getLoadedAlgorithms()) {
			if (algorithm.getType() == GraphicType) {
				specificAlgorithms.add(algorithm);
			}
		}

		Algorithm<?>[] convertedToArray = new Algorithm<?>[specificAlgorithms.size()];
		specificAlgorithms.toArray(convertedToArray);
		return convertedToArray;
	}

	/**
	 * @return the loadedSchedulers
	 */
	public Scheduler<?>[] getLoadedSchedulers() {

		if (loadedSchedulers == null) {
			loadedSchedulers = new ArrayList<Scheduler<?>>();
			for (Plugin plugin : getPlugins(Scheduler.class)) {
				loadedSchedulers.add((Scheduler<?>) plugin);
			}
		}
		Scheduler<?>[] convertedToArray = new Scheduler<?>[loadedSchedulers.size()];
		loadedSchedulers.toArray(convertedToArray);
		return convertedToArray;
	}

	/**
	 * @return the loadedVisibilities
	 */
	private ArrayList<Visibility<?>> getLoadedVisibilities() {

		if (loadedVisibilities == null) {
			loadedVisibilities = new ArrayList<Visibility<?>>();
			for (Plugin plugin : getPlugins(Visibility.class)) {
				loadedVisibilities.add((Visibility<?>) plugin);
			}
		}
		return loadedVisibilities;
	}

	public Visibility<?>[] getLoadedVisibilities(TYPE GraphicType) {

		ArrayList<Visibility<?>> specificVisibilities = new ArrayList<Visibility<?>>();
		for (Visibility<?> visibility : getLoadedVisibilities()) {

			if (visibility == null)
				continue;
			if (visibility.getType() == GraphicType)
				specificVisibilities.add(visibility);
		}

		Visibility<?>[] convertToArray = new Visibility<?>[specificVisibilities.size()];
		specificVisibilities.toArray(convertToArray);

		return convertToArray;
	}

	/**
	 * @return the loadedInitialConditions
	 */
	private ArrayList<InitialConditions<?>> getLoadedInitialConditions() {

		if (loadedInitialConditions == null) {
			loadedInitialConditions = new ArrayList<InitialConditions<?>>();
			for (Plugin plugin : getPlugins(InitialConditions.class)) {
				loadedInitialConditions.add((InitialConditions<?>) plugin);
			}
		}
		return loadedInitialConditions;
	}

	public InitialConditions<?>[] getLoadedInitialConditions(TYPE GraphicType) {

		ArrayList<InitialConditions<?>> specificInitialConditions = new ArrayList<InitialConditions<?>>();
		for (InitialConditions<?> initialConditions : getLoadedInitialConditions()) {

			if (initialConditions == null)
				continue;
			if (initialConditions.getType() == GraphicType)
				specificInitialConditions.add(initialConditions);
		}

		InitialConditions<?>[] convertedArray = new InitialConditions<?>[specificInitialConditions.size()];
		loadedInitialConditions.toArray(convertedArray);
		return convertedArray;
	}

	/**
	 * @return the loadedAgreements
	 */
	private ArrayList<Agreement<?>> getLoadedAgreements() {

		if (loadedAgreements == null) {
			loadedAgreements = new ArrayList<Agreement<?>>();
			for (Plugin plugin : getPlugins(Agreement.class)) {
				loadedAgreements.add((Agreement<?>) plugin);
			}
		}
		return loadedAgreements;
	}

	public Agreement<?>[] getLoadedAgreements(TYPE GraphicType) {

		ArrayList<Agreement<?>> specificAgreements = new ArrayList<Agreement<?>>();
		for (Agreement<?> agreement : getLoadedAgreements()) {

			if (agreement == null)
				continue;
			if (agreement.getType() == GraphicType)
				specificAgreements.add(agreement);
		}

		Agreement<?>[] convertToArray = new Agreement<?>[specificAgreements.size()];
		specificAgreements.toArray(convertToArray);
		return convertToArray;
	}

	/**
	 * @return the loadedMemories
	 */
	private ArrayList<Memory<?>> getLoadedMemories() {

		if (loadedMemories == null) {
			loadedMemories = new ArrayList<Memory<?>>();
			for (Plugin plugin : getPlugins(Memory.class)) {
				loadedMemories.add((Memory<?>) plugin);
			}
		}
		return loadedMemories;
	}

	public Memory<?>[] getLoadedMemories(TYPE GraphicType) {

		ArrayList<Memory<?>> specificMemories = new ArrayList<Memory<?>>();
		for (Memory<?> memory : getLoadedMemories()) {

			if (memory == null)
				continue;
			if (memory.getType() == GraphicType)
				specificMemories.add(memory);
		}

		Memory<?>[] convertedToArray = new Memory<?>[specificMemories.size()];
		specificMemories.toArray(convertedToArray);
		return convertedToArray;
	}

	/**
	 * @return the loadedMemories
	 */
	public Measure[] getLoadedMeasures() {

		if (loadedMeasures == null) {
			loadedMeasures = new ArrayList<Measure>();
			for (Plugin plugin : getPlugins(Measure.class)) {
				loadedMeasures.add((Measure) plugin);
			}
		}

		Measure[] convertedToArray = new Measure[loadedMeasures.size()];
		loadedMeasures.toArray(convertedToArray);
		return convertedToArray;
	}
}
