/**
 * 
 */
package uo.harish.prakash.sycamore;

import java.io.File;
import uo.harish.prakash.sycamore.batch.gui.SycamoreBatchMainWindow;

/**
 * @author harry
 *
 */
public class Execute {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		new SycamoreBatchMainWindow(new File("/home/harry/Documents/Sycamore"));
	}

}
